## Instalação

Com NPM

```bash
npm install
```

Com Yarn

```bash
yarn
```

## Uso

### Executar aplicação ReactJS

```bash
yarn start react-shop
```

## Testes

### Executar todos os testes

```bash
yarn test-all
```
