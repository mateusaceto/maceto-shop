import { CartType } from '@maceto-shop/data-types';
import { LocalDBService } from '../localDB/LocalDBService';

export class CartService extends LocalDBService<CartType> {
  schema: string = 'cart';
  static instance: CartService = new CartService();
}
