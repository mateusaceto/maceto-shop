import localForage from 'localforage';

/**
 * @description simple abstract DB using localForage library.
 */
export abstract class LocalDBService<T extends { id?: string }> {
  schema: string = '';

  async get(): Promise<Map<string, T>> {
    try {
      const value = await localForage.getItem<Map<string, T>>(this.schema);
      return Promise.resolve(value ?? new Map());
    } catch (err) {
      return Promise.reject(err);
    }
  }

  async set(item: T): Promise<Map<string, T>> {
    try {
      if (!item?.id) return Promise.reject('id is required');
      const data = await this.get();
      data.set(item.id, item);
      await localForage.setItem(this.schema, data);
      return Promise.resolve(data);
    } catch (err) {
      return Promise.reject(err);
    }
  }

  async remove(id: string): Promise<Map<string, T>> {
    try {
      if (!id) return Promise.reject('id is required');
      const data = await this.get();
      data.delete(id);
      await localForage.setItem(this.schema, data);
      return Promise.resolve(data);
    } catch (err) {
      return Promise.reject(err);
    }
  }

  async reset() {
    try {
      await localForage.clear();
      return Promise.resolve('ok');
    } catch (err) {
      return Promise.reject(err);
    }
  }
}
