import { CartService } from './cart/CartService';

/**
 * @description Central Services Factory
 */

export class ServiceFactory {
  static cartService = () => CartService.instance;
}
