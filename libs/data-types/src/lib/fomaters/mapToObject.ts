export function mapToObj<T>(inputMap: Map<string, T>) {
  const obj: { [key: string]: T } = {};

  inputMap.forEach(function (value, key) {
    obj[key] = value;
  });

  return obj;
}
