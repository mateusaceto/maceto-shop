export function toMoney(str: string): string {
  try {
    return parseFloat(str).toLocaleString('pt-br', {
      style: 'currency',
      currency: 'BRL',
    });
  } catch (err) {
    return str;
  }
}
