export interface ProductType {
  id: string;
  createdAt: string;
  name: string;
  price: string;
  image: string;
  stock: number;
  quantity?: number;
}

export interface CartType {
  id?: string;
  product: ProductType;
  quantity: number;
}
