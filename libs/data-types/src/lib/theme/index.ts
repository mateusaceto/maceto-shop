export interface PalleteType {
  light: string;
  main: string;
  dark: string;
}

export interface ThemeType {
  pallete: {
    primary: PalleteType;
    secondary: PalleteType;
  };
  spacing: (space: number) => number;
}

export const theme: ThemeType = {
  pallete: {
    primary: {
      light: '#9575cd',
      main: '#673ab7',
      dark: '#4527a0',
    },
    secondary: {
      light: '#616161 ',
      main: '#212121',
      dark: '#000000',
    },
  },
  spacing: (space: number) => space * 5,
};
