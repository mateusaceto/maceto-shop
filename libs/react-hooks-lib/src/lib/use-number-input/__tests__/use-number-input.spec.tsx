import { renderHook, act } from '@testing-library/react-hooks';
import useNumberInput from '../use-number-input';

// value,
//   {
//     handleInputChange,
//     handlePlus,
//     handleMinus,
//     setValue,
//   },
describe('UseNumberInput', () => {
  it('should starts with 1', () => {
    const { result } = renderHook(() => useNumberInput());

    expect(result.current[0]).toBe(1);
  });

  it('should work correctly, change value, increment and decrement', () => {
    const { result } = renderHook(() => useNumberInput());

    act(() => {
      result.current[1].handleInputChange(20);
    });

    expect(result.current[0]).toBe(20);

    act(() => {
      result.current[1].handlePlus();
    });

    act(() => {
      result.current[1].handlePlus();
    });

    expect(result.current[0]).toBe(22);

    act(() => {
      result.current[1].handleMinus();
    });

    act(() => {
      result.current[1].handleMinus();
    });

    expect(result.current[0]).toBe(20);
  });

  it('value should not to be less than one', () => {
    const { result } = renderHook(() => useNumberInput());

    expect(result.current[0]).toBe(1);

    act(() => {
      result.current[1].handleMinus();
    });

    act(() => {
      result.current[1].handleMinus();
    });

    expect(result.current[0]).toBe(1);

    act(() => {
      result.current[1].handleInputChange(-20);
    });

    expect(result.current[0]).toBe(1);

    act(() => {
      result.current[1].handleInputChange('-10');
    });

    expect(result.current[0]).toBe(1);
  });
});
