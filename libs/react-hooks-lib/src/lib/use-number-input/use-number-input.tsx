import { useCallback, useState } from 'react';

interface UseNumberInputActions {
  handleInputChange: (value: number | string) => void;
  handlePlus: () => void;
  handleMinus: () => void;
  setValue: React.Dispatch<React.SetStateAction<string | number>>;
}

export function useNumberInput(
  initialValue?: number | string
): [value: number | string, actions: UseNumberInputActions] {
  const [value, setValue] = useState<number | string>(initialValue ?? 1);

  const handleInputChange = useCallback(
    (newValue: number | string) => {
      if (newValue === '') {
        setValue(1);
        return;
      }
      if (!newValue || (newValue as number) < 1) {
        setValue(1);
        return;
      }
      setValue(newValue);
    },
    [setValue]
  );

  const handlePlus = useCallback(() => {
    setValue(parseInt(value as string) + 1);
  }, [value, setValue]);

  const handleMinus = useCallback(() => {
    setValue(
      parseInt(value as string) - 1 < 1 ? 1 : parseInt(value as string) - 1
    );
  }, [value, setValue]);

  return [
    value,
    {
      handleInputChange,
      handlePlus,
      handleMinus,
      setValue,
    },
  ];
}

export default useNumberInput;
