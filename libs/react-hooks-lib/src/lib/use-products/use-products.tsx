import { ProductType } from '@maceto-shop/data-types';
import useFetch, { UseFetchPropsActions } from '../use-fetch/use-fetch';

export const ProductsApiUrl =
  'https://61ef5197d593d20017dbb453.mockapi.io/api/v1/products';

export function useProducts(): [
  data: ProductType[],
  actions: UseFetchPropsActions
] {
  const [data, { error, loading }] = useFetch<ProductType>(ProductsApiUrl);

  return [data, { error, loading }];
}

export default useProducts;
