import { useCallback, useEffect, useState } from 'react';

export interface UseFetchPropsActions {
  error: unknown;
  loading: boolean;
}

export function useFetch<T>(
  url: string
): [data: T[], actions: UseFetchPropsActions] {
  const [data, setData] = useState<T[]>([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState<boolean>(true);

  const getData = useCallback(async () => {
    try {
      setLoading(true);
      const res = await fetch(url);
      const newData: T[] = await res.json();
      setData(newData);
      setError(null);
    } catch (err) {
      console.log(err);
    } finally {
      setLoading(false);
    }
  }, [url]);

  useEffect(() => {
    getData();
  }, [getData, url]);

  return [
    data,
    {
      error,
      loading,
    },
  ];
}

export default useFetch;
