import { ProductType } from '@maceto-shop/data-types';
import { renderHook } from '@testing-library/react-hooks';
import useFetch from '../use-fetch';

const ProductMockData: ProductType[] = [
  {
    id: '1',
    createdAt: '2019-09-02T12:58:54.103Z',
    name: 'Rustic Metal Fish',
    price: '289.00',
    image: 'http://lorempixel.com/640/480/food',
    stock: 65171,
  },
  {
    id: '2',
    createdAt: '2019-09-02T07:59:58.181Z',
    name: 'Sleek Wooden Soap',
    price: '430.00',
    image: 'http://lorempixel.com/640/480/transport',
    stock: 91260,
  },
  {
    id: '3',
    createdAt: '2019-09-02T22:14:05.454Z',
    name: 'Small Cotton Shoes',
    price: '993.00',
    image: 'http://lorempixel.com/640/480/sports',
    stock: 36608,
  },
];

const ProductsFaker = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve(ProductMockData),
  })
);

describe('UseFetch', () => {
  it('should render successfully', async () => {
    /* eslint-disable-next-line */
    const globalRef: any = global;
    globalRef.fetch = ProductsFaker;

    const { result, waitForNextUpdate } = renderHook(() =>
      useFetch<ProductType>('https://api.com/products')
    );

    await waitForNextUpdate();

    expect(result.current[0].length).toBe(ProductMockData.length);
  });
});
