import { CartType, ProductType } from '@maceto-shop/data-types';
import {
  getRequest,
  RootState,
  setRequest,
  setResetRequest,
} from '@maceto-shop/store';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

export interface useCartActions {
  addToCart: (product: ProductType, quantity: number) => void;
  changeCartQuantity: (product: ProductType, quantity: number) => void;
  removeCartItem: (product: ProductType) => void;
  reset: () => void;
}

export function useCart(): [
  cart: Map<string, CartType>,
  actions: useCartActions
] {
  const cart = useSelector((state: RootState) => state.cart.data);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getRequest());
  }, [dispatch]);

  const addToCart = (product: ProductType, quantity: number) =>
    dispatch(setRequest({ product, quantity, method: 'STORE' }));

  const removeCartItem = (product: ProductType) =>
    dispatch(setRequest({ product, method: 'DELETE' }));

  const changeCartQuantity = (product: ProductType, quantity: number) =>
    dispatch(setRequest({ product, quantity, method: 'PUT' }));

  const reset = () => dispatch(setResetRequest());

  return [cart, { addToCart, removeCartItem, changeCartQuantity, reset }];
}

export default useCart;
