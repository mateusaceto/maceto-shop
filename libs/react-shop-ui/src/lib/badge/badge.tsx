import { Box } from '@fower/react';

export interface BadgeProps {
  children: React.ReactNode | React.ReactNode[];
  quantity: number;
  onClick?: () => void;
}

export const Badge: React.FC<BadgeProps> = ({
  children,
  quantity,
  onClick,
}) => {
  return (
    <Box relative onClick={onClick}>
      <Box>{children}</Box>
      {quantity > 0 && (
        <Box
          absolute
          zIndex-20
          top-15
          right-0
          textXS
          circle5
          shadowMedium
          bgRed900
          white
          fontSans
          flex
          alignItems="center"
          justifyContent="center"
          opacity={80}
        >
          {quantity}
        </Box>
      )}
    </Box>
  );
};

export default Badge;
