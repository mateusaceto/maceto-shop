import { Box } from '@fower/react';
import { ProductType, toMoney } from '@maceto-shop/data-types';
import { useNumberInput } from '@maceto-shop/react-hooks-lib';
import React, { useCallback, useEffect } from 'react';
import { Button } from '../button/button';
import { NumberInput } from '../number-input/number-input';

export interface CartCardProps {
  onRemove: () => void;
  onChange: (value: number) => void;
}

export const CartCard: React.FC<CartCardProps & ProductType> = ({
  id,
  name,
  image,
  price,
  quantity,
  onRemove,
  onChange,
}) => {
  const [value, { handleInputChange, handlePlus, handleMinus }] =
    useNumberInput(quantity);

  const handleRemoveToCart = useCallback(() => {
    onRemove();
  }, [onRemove]);

  useEffect(() => {
    onChange(value as number);
  }, [value]);

  return (
    <Box
      id={`CartCard${id}`}
      toCenterY
      p-10
      w-250
      minH={350}
      bgWhite
      rounded-5
      shadowMedium
      fontSans
      column
      alignItems="center"
      justifyContent="center"
      gap={6}
    >
      <Box as="span" textXL fontBold textCenter>
        {name}
      </Box>
      <Box as="img" src={image} square-10rem roundedSmall />
      <Box as="span" fontBold data-testid="product-price">
        {toMoney((parseFloat(price) * (value as number)).toString())}
      </Box>
      <NumberInput
        onPlus={handlePlus}
        onMinus={handleMinus}
        onChange={handleInputChange}
        value={value}
      />
      <Button
        text="Remover do carrinho"
        secondary
        onClick={handleRemoveToCart}
      />
    </Box>
  );
};

export default CartCard;
