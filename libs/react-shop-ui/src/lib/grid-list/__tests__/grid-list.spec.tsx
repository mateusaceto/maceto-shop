import { render, screen } from '@testing-library/react';
import MockProvider from '../../../MockProvider';

import GridList from '../grid-list';

interface ItemType {
  id: string;
  title: string;
  email: string;
}

const mockItems: ItemType[] = [
  {
    id: '1',
    title: 'Test 1',
    email: 'Email 1',
  },
  {
    id: '2',
    title: 'Test 2',
    email: 'Email 2',
  },
  {
    id: '3',
    title: 'Test 3',
    email: 'Email 3',
  },
];

const MockGrid: React.FC = () => (
  <MockProvider>
    <GridList
      items={mockItems}
      render={(item) => (
        <div role="contentinfo">
          <span>{item.id}</span>
          <span>{item.title}</span>
          <span>{item.email}</span>
        </div>
      )}
    />
  </MockProvider>
);

describe('Grid List Component', () => {
  it('should render successfully', () => {
    const { baseElement, asFragment } = render(<MockGrid />);
    expect(baseElement).toBeTruthy();
    expect(asFragment()).toMatchSnapshot();
  });

  it('should render items correctly', () => {
    render(<MockGrid />);
    const renderedElements = screen.getAllByRole('contentinfo');
    expect(renderedElements.length).toBe(3);

    mockItems.forEach((item: ItemType) => {
      Object.keys(item).forEach((key) => {
        const renderedElementItem = screen.getByText(
          item[key as keyof ItemType]
        );
        expect(renderedElementItem).toBeTruthy();
      });
    });
  });
});
