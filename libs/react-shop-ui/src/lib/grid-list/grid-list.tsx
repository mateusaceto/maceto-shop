import { Box } from '@fower/react';

export interface GridListProps<T> {
  items: T[];
  render: (item: T) => React.ReactNode;
}

export function GridList<T>({ items, render }: GridListProps<T>) {
  return (
    <Box
      row
      flexWrap="wrap"
      gap={10}
      w="100%"
      alignItems="center"
      justifyContent="center"
    >
      {items.map((item, index) => (
        <Box key={`gridItem${index}`}>{render(item)}</Box>
      ))}
    </Box>
  );
}

export default GridList;
