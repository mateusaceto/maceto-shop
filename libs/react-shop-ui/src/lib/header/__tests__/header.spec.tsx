import { render, screen } from '@testing-library/react';
import MockProvider from '../../../MockProvider';
import Header, { HeaderProps } from '../header';

const MockHeader: React.FC<HeaderProps> = ({ ...props }) => (
  <MockProvider>
    <Header {...props} />
  </MockProvider>
);

describe('Header', () => {
  it('should render successfully', () => {
    const { baseElement, asFragment } = render(<MockHeader />);
    expect(baseElement).toBeTruthy();
  });

  it('should render header correctly with title', () => {
    render(<MockHeader title="A great header" />);
    expect(screen.getByText('A great header')).toBeTruthy();
  });

  it('should render header correctly with title and action', () => {
    render(<MockHeader title="A great header" actions={<span>Action</span>} />);
    expect(screen.getByText('A great header')).toBeTruthy();
    expect(screen.getByText('Action')).toBeTruthy();
  });
});
