import React from 'react';
import { Box } from '@fower/react';
import { styled } from '@fower/styled';
import StyledHeader from './styles';

export interface HeaderProps {
  title?: string;
  actions?: React.ReactNode;
  secondary?: boolean;
  primary?: boolean;
}

export function Header({ title, primary, secondary, actions }: HeaderProps) {
  const HeaderBox = styled(StyledHeader);

  return (
    <>
      <HeaderBox
        role="heading"
        w-full
        shadowMedium
        white
        fontSans
        textLG
        fontBold
        row
        justifyContent="space-between"
        alignItems="center"
        secondary={secondary}
        primary={primary}
        fixed
        w="100%"
      >
        {title ?? ''}
        {actions}
      </HeaderBox>
      <Box h={60} w="100%" />
    </>
  );
}

export default Header;
