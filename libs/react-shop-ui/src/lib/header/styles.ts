import styled from 'styled-components';
import { ThemeType } from '@maceto-shop/data-types';

const StyledHeader = styled.div`
  padding: ${(props: { theme: ThemeType }) => props.theme.spacing(3)}px;
  background-color: ${(props: { theme: ThemeType }) =>
    props.theme.pallete.primary.main};
`;

export default StyledHeader;
