import { Box } from '@fower/react';

export interface ContainerProps {
  children: React.ReactNode | React.ReactNode[] | null;
}

export function Container({ children }: ContainerProps) {
  return (
    <Box
      w="100%"
      h="100%"
      bgGray100
      pb={120}
      pt5
      row
      alignItems="center"
      justifyContent="center"
    >
      {children}
    </Box>
  );
}

export default Container;
