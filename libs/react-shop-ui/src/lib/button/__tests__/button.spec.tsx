import { render, screen } from '@testing-library/react';

import Button from '../button';
import MockProvider from '../../../MockProvider';

describe('Button', () => {
  it('should render successfully', () => {
    const { baseElement, asFragment } = render(
      <MockProvider>
        <Button />
      </MockProvider>
    );
    expect(baseElement).toBeTruthy();
    expect(asFragment()).toMatchSnapshot();
  });

  it('should render children correctly', () => {
    render(
      <MockProvider>
        <Button>
          <span role="contentinfo">Test Children</span>
        </Button>
      </MockProvider>
    );

    const childElement = screen.getByRole('contentinfo');

    expect(childElement.textContent).toBe('Test Children');
  });

  it('should render text correctly', () => {
    render(
      <MockProvider>
        <Button text="Test text" />
      </MockProvider>
    );

    const buttonElement = screen.getByText('Test text');
    expect(buttonElement.textContent).toBe('Test text');
  });
});
