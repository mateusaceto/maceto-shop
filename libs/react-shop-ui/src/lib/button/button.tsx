import { Box } from '@fower/react';
import StyledButton from './styles';

export interface ButtonProps {
  children?: React.ReactNode | React.ReactNode[] | null;
  secondary?: boolean;
  primary?: boolean;
  text?: string;
  transparentBtn?: boolean;
}

export const Button: React.FC<
  React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > &
    ButtonProps
> = ({ children, text, ...rest }) => {
  return (
    <Box opacity-95--hover scale-105--active>
      <StyledButton {...rest} ref={null}>
        {text ?? children}
      </StyledButton>
    </Box>
  );
};

export default Button;
