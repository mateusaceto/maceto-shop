import styled from 'styled-components';
import { ThemeType } from '@maceto-shop/data-types';

const StyledButton = styled.button`
  padding: ${(props: { theme: ThemeType }) => props.theme.spacing(1.5)}px;
  color: white;
  min-width: 35px;
  min-height: 35px;
  border-radius: ${(props: { theme: ThemeType }) => props.theme.spacing(1)}px;
  box-shadow: rgb(0 0 0 / 12%) 0px 4px 8px, rgb(0 0 0 / 2%) 0px 0px 2px;
  background-color: ${(
    props: { theme: ThemeType } & {
      secondary?: boolean;
      primary?: boolean;
      transparentBtn?: boolean;
    }
  ) =>
    props.transparentBtn
      ? 'transparent'
      : props.theme.pallete[props.secondary ? 'secondary' : 'primary'].main};
`;

export default StyledButton;
