import { Box } from '@fower/react';
import { ProductType, toMoney } from '@maceto-shop/data-types';
import { useNumberInput } from '@maceto-shop/react-hooks-lib';
import React, { useCallback } from 'react';
import { Button } from '../button/button';
import { NumberInput } from '../number-input/number-input';

export interface ProductCardProps {
  onAdd: (quantity: number) => void;
}

export const ProductCard: React.FC<ProductCardProps & ProductType> = ({
  id,
  name,
  image,
  price,
  onAdd,
}) => {
  const [value, { handleInputChange, handlePlus, handleMinus, setValue }] =
    useNumberInput();

  const handleAddToCart = useCallback(() => {
    onAdd(parseInt(value as string));
    setValue(1);
  }, [onAdd, value, setValue]);

  return (
    <Box
      id={`productCard${id}`}
      toCenterY
      p-10
      w-250
      minH={350}
      bgWhite
      rounded-5
      shadowMedium
      fontSans
      column
      alignItems="center"
      justifyContent="center"
      gap={6}
    >
      <Box as="span" textXL fontBold textCenter>
        {name}
      </Box>
      <Box as="img" src={image} square-10rem roundedSmall />
      <Box as="span" fontBold data-testid="product-price">
        {toMoney(price)}
      </Box>
      <NumberInput
        onPlus={handlePlus}
        onMinus={handleMinus}
        onChange={handleInputChange}
        value={value}
      />
      <Button
        text="Adicionar ao carrinho"
        secondary
        onClick={handleAddToCart}
      />
    </Box>
  );
};

export default ProductCard;
