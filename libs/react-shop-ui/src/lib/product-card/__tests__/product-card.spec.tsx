import '@testing-library/jest-dom';
import { ProductType, toMoney } from '@maceto-shop/data-types';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import MockProvider from '../../../MockProvider';
import ProductCard, { ProductCardProps } from '../product-card';

const ProductCardMock: React.FC<ProductCardProps & ProductType> = ({
  ...props
}) => (
  <MockProvider>
    <ProductCard {...props} />
  </MockProvider>
);

const MockProduct: ProductType = {
  id: '1',
  name: 'Product 1',
  image: '',
  price: '100.9',
  createdAt: '',
  stock: 1000,
};

describe('ProductCard', () => {
  it('should render successfully', () => {
    const { baseElement, asFragment } = render(
      <ProductCardMock {...MockProduct} onAdd={() => null} />
    );
    expect(baseElement).toBeTruthy();
    expect(asFragment()).toMatchSnapshot();
  });

  it('render product details correctly', () => {
    render(<ProductCardMock {...MockProduct} onAdd={() => null} />);
    expect(screen.getByText(MockProduct.name)).toBeInTheDocument();

    const priceElement = screen.getByTestId('product-price');
    expect(priceElement.textContent).toBe(toMoney(MockProduct.price));

    const imageElement = screen.getByRole('img');
    expect(imageElement).toBeInTheDocument();
  });

  it('expect product input starts with 1', () => {
    render(<ProductCardMock {...MockProduct} onAdd={() => null} />);
    const inputElement = screen.getByTestId('styled-input');
    expect(inputElement).toHaveAttribute('value', '1');
  });

  describe('ProductCard Integrations Tests', () => {
    it('expect product counter works', () => {
      const mockAdd = jest.fn((quantity: number) => null);
      render(<ProductCardMock {...MockProduct} onAdd={mockAdd} />);

      const inputElement = screen.getByTestId('styled-input');
      const minusBtnElement = screen.getByText('-');
      const plusBtnElement = screen.getByText('+');

      expect(inputElement).toHaveAttribute('value', '1');
      fireEvent.click(plusBtnElement);
      expect(inputElement).toHaveAttribute('value', '2');
      fireEvent.click(plusBtnElement);
      expect(inputElement).toHaveAttribute('value', '3');

      fireEvent.click(minusBtnElement);
      expect(inputElement).toHaveAttribute('value', '2');
      fireEvent.click(minusBtnElement);
      expect(inputElement).toHaveAttribute('value', '1');
      fireEvent.click(minusBtnElement);
      expect(inputElement).toHaveAttribute('value', '1');
      fireEvent.change(inputElement, { target: { value: '20' } });
      fireEvent.click(plusBtnElement);
      fireEvent.click(plusBtnElement);
      expect(inputElement).toHaveAttribute('value', '22');
    });

    it('expect add to cart works correctly', () => {
      const mockAdd = jest.fn((quantity: number) => null);
      render(<ProductCardMock {...MockProduct} onAdd={mockAdd} />);
      const inputElement = screen.getByTestId('styled-input');
      const minusBtnElement = screen.getByText('-');
      const plusBtnElement = screen.getByText('+');
      const addBtnElement = screen.getByText('Adicionar ao carrinho');

      fireEvent.change(inputElement, { target: { value: '20' } });
      fireEvent.click(plusBtnElement);
      fireEvent.click(plusBtnElement);
      fireEvent.click(minusBtnElement);
      fireEvent.click(addBtnElement);

      expect(inputElement).toHaveAttribute('value', '1');
      expect(mockAdd.mock.calls[0][0]).toBe(21);
    });
  });
});
