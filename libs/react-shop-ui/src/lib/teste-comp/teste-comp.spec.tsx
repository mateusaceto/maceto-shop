import { render } from '@testing-library/react';

import TesteComp from './teste-comp';

describe('TesteComp', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<TesteComp />);
    expect(baseElement).toBeTruthy();
  });
});
