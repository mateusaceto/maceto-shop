import styled from 'styled-components';
import { ThemeType } from '@maceto-shop/data-types';

const StyledInput = styled.input`
  padding: ${(props: { theme: ThemeType }) => props.theme.spacing(1.5)}px;
  color: black;
  border-radius: 3px;
  border: 1.5px solid #9e9e9e;
  width: 100%;

  &:focus {
    outline: 1px solid
      ${(
        props: { theme: ThemeType } & { secondary?: boolean; primary?: boolean }
      ) => props.theme.pallete[props.secondary ? 'secondary' : 'primary'].main};
    border-color: ${(
      props: { theme: ThemeType } & { secondary?: boolean; primary?: boolean }
    ) => props.theme.pallete[props.secondary ? 'secondary' : 'primary'].main};
  }

  &:hover {
    border-color: ${(
      props: { theme: ThemeType } & { secondary?: boolean; primary?: boolean }
    ) => props.theme.pallete[props.secondary ? 'secondary' : 'primary'].dark};
  }
`;

export default StyledInput;
