import StyleInput from './styles';
import { useRef } from 'react';

/* eslint-disable-next-line */
export interface InputProps {}

const Input: React.FC<
  React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  > &
    InputProps
> = ({ ...rest }) => {
  const inputElement = useRef<HTMLInputElement | null>(null);
  return <StyleInput {...rest} ref={inputElement} data-testid="styled-input" />;
};

export default Input;
