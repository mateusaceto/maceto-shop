import '@testing-library/jest-dom';
import { fireEvent, render, screen } from '@testing-library/react';
import MockProvider from '../../../MockProvider';
import Input from '../input';

const MockInput: React.FC = () => (
  <MockProvider>
    <Input />
  </MockProvider>
);

describe('Input', () => {
  it('should render successfully', () => {
    const { baseElement, asFragment } = render(<MockInput />);
    expect(baseElement).toBeTruthy();
    expect(asFragment()).toMatchSnapshot();
  });

  it('Expect input to work correctly', () => {
    const { getByText } = render(<MockInput />);
    const inputElement = screen.getByTestId('styled-input');
    expect(inputElement).toBeTruthy();
    fireEvent.change(inputElement, { target: { value: 'Hello World!' } });
    expect(screen.getByDisplayValue('Hello World!')).toBeInTheDocument();
  });
});
