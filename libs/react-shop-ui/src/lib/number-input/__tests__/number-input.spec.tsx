import '@testing-library/jest-dom';
import { fireEvent, render, screen, act } from '@testing-library/react';
import MockProvider from '../../../MockProvider';

import NumberInput, { NumberInputProps } from '../number-input';

const MockNumberInput: React.FC<NumberInputProps> = ({ ...props }) => (
  <MockProvider>
    <NumberInput {...props} />
  </MockProvider>
);

describe('NumberInput', () => {
  it('should render successfully', () => {
    const { baseElement, asFragment } = render(
      <MockNumberInput
        onPlus={() => null}
        onMinus={() => null}
        onChange={() => null}
        value={10}
      />
    );
    expect(baseElement).toBeTruthy();
    expect(asFragment()).toMatchSnapshot();
  });

  it('should call the function props on fire events and not accept non numbers', () => {
    const mockOnPlus = jest.fn(() => null);
    const mockOnMinus = jest.fn(() => null);
    const mockOnChange = jest.fn((value: string | number) => null);

    render(
      <MockNumberInput
        onPlus={mockOnPlus}
        onMinus={mockOnMinus}
        onChange={mockOnChange}
        value={1}
      />
    );

    const minusBtnElement = screen.getByText('-');
    const plusBtnElement = screen.getByText('+');
    const inputElement = screen.getByTestId('styled-input');

    act(() => {
      fireEvent.click(minusBtnElement);
      fireEvent.click(plusBtnElement);
      fireEvent.change(inputElement, { target: { value: '20' } });
    });

    expect(mockOnChange).toHaveBeenCalled();
    expect(mockOnChange.mock.calls[0][0]).toBe('20');
    expect(mockOnPlus).toHaveBeenCalledTimes(1);
    expect(mockOnMinus).toHaveBeenCalledTimes(1);

    fireEvent.change(inputElement, { target: { value: 'Hello' } });
    expect(screen.queryByDisplayValue('Hello')).not.toBeInTheDocument();
  });
});
