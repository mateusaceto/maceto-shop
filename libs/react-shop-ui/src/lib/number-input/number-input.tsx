import { Box } from '@fower/react';
import { useCallback } from 'react';
import { Button } from '../button/button';
import Input from '../input/input';

/* eslint-disable-next-line */
export interface NumberInputProps {
  onPlus: () => void;
  onMinus: () => void;
  onChange: (value: string | number) => void;
  value: string | number;
}

export function NumberInput({
  onPlus,
  onMinus,
  onChange,
  value,
}: NumberInputProps) {
  const handleChange = useCallback(
    (evt: React.ChangeEvent<HTMLInputElement>) => {
      onChange(evt.target?.value);
    },
    [onChange]
  );

  return (
    <Box w-140 row gap={4}>
      <Button onClick={onMinus}>-</Button>
      <Input type="number" min="1" value={value} onChange={handleChange} />
      <Button onClick={onPlus}>+</Button>
    </Box>
  );
}

export default NumberInput;
