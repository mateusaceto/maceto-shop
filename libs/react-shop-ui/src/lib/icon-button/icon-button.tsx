import { styled } from '@fower/styled';
import { IconType } from 'react-icons';

export interface IconButtonProps {
  icon: IconType | React.ReactNode;
}

export const IconButton: React.FC<
  React.DetailedHTMLProps<
    React.ButtonHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > &
    IconButtonProps
> = ({ icon, ...rest }) => {
  const StyledButton = styled('button');

  return (
    <StyledButton
      {...rest}
      circle9
      cursorPointer
      shadowSmall--hover
      scale-105--active
      fontSans
      textXL
      white
      bgTransparent
    >
      {icon}
    </StyledButton>
  );
};

export default IconButton;
