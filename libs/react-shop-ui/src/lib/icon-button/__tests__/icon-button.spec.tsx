import { render, screen } from '@testing-library/react';
import MockProvider from '../../../MockProvider';
import IconButton from '../icon-button';
import { FaShoppingCart } from 'react-icons/fa';

const MockIconButton: React.FC = () => (
  <MockProvider>
    <IconButton icon={<FaShoppingCart data-testid="cart-icon" />} />
  </MockProvider>
);

describe('IconButton', () => {
  it('should render successfully', () => {
    const { baseElement, asFragment } = render(<MockIconButton />);
    expect(baseElement).toBeTruthy();
    expect(asFragment()).toMatchSnapshot();
  });

  it('should render Icon Button correctly', () => {
    render(<MockIconButton />);
    expect(screen.getByTestId('cart-icon')).toBeTruthy();
  });
});
