import { theme } from '@maceto-shop/data-types';
import { ThemeProvider } from 'styled-components';

export interface MockProviderProps {
  children?: React.ReactNode | React.ReactNode[] | null;
}

export const MockProvider: React.FC<MockProviderProps> = ({ children }) => {
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};

export default MockProvider;
