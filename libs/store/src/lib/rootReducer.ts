import { combineReducers } from '@reduxjs/toolkit';
import { cartSlice } from './cart';

/**
 * @description App root reducer.
 */
export const rootReducer = combineReducers({
  cart: cartSlice.reducer,
});

export type RootState = ReturnType<typeof rootReducer>;
