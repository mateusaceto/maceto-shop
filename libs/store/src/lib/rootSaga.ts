import { all, call, spawn } from 'redux-saga/effects';
import { cartSaga } from './cart';

export function* rootSaga() {
  const sagas = [cartSaga];
  yield all(
    sagas.map((saga) =>
      spawn(function* () {
        while (true) {
          try {
            yield call(saga);
            break;
          } catch (e) {
            //
          }
        }
      })
    )
  );
}
