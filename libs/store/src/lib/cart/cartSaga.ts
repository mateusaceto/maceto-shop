import { CartType } from '@maceto-shop/data-types';
import { ServiceFactory } from '@maceto-shop/services';
import { all, put, select, takeLatest } from 'redux-saga/effects';
import { RootState } from '../rootReducer';
import {
  CartActionTypes,
  CartTypes,
  getFailure,
  getSuccess,
  setFailure,
  setSuccess,
} from './cartSlice';
import { toast } from 'react-toastify';
import { setResetSuccess } from '.';

function* getCart() {
  try {
    const CartService = ServiceFactory.cartService();
    const data: Map<string, CartType> = yield CartService.get();
    yield put(
      getSuccess({
        data,
      })
    );
  } catch (error) {
    yield put(
      getFailure({
        error: String(error),
      })
    );
  }
}

function* setCart(action: CartActionTypes['setRequest']) {
  try {
    const CartService = ServiceFactory.cartService();
    const cartState: Map<string, CartType> = yield select(
      (state: RootState) => state.cart.data
    );

    const newCartState = new Map(cartState);

    switch (action.payload.method) {
      case 'STORE':
        if (action.payload.product && action.payload.quantity) {
          const { product } = action.payload;
          let { quantity } = action.payload;
          if (newCartState.has(product.id)) {
            const currentCart = newCartState.get(product.id);
            quantity += currentCart?.quantity as number;
          }
          yield CartService.set({ product, quantity, id: product.id });
          newCartState.set(product.id, { product, quantity });
          toast('Produto adicionado ao carrinho', { type: 'success' });
        }
        break;
      case 'PUT':
        if (action.payload.product && action.payload.quantity) {
          const { product, quantity } = action.payload;
          yield CartService.set({ product, quantity, id: product.id });
          newCartState.set(product.id, { product, quantity });
        }
        break;
      case 'DELETE':
        if (!action.payload.product) break;
        yield CartService.remove(action.payload?.product?.id);
        newCartState.delete(action.payload.product.id);
        toast('Produto removido do carrinho', { type: 'success' });
        break;
      default:
        break;
    }

    yield put(setSuccess({ data: newCartState }));
  } catch (error) {
    yield put(
      setFailure({
        error: String(error),
      })
    );
  }
}

function* reset() {
  try {
    const CartService = ServiceFactory.cartService();
    yield CartService.reset();
    yield put(setResetSuccess());
    toast.success('Pedido realizado com sucesso!', { type: 'success' });
  } catch (error) {
    yield put(
      setFailure({
        error: String(error),
      })
    );
  }
}

export function* cartSaga() {
  yield all([
    takeLatest<CartTypes>('@cart/getRequest', getCart),
    takeLatest<CartTypes, any>('@cart/setRequest', setCart),
    takeLatest<CartTypes, any>('@cart/setResetRequest', reset),
  ]);
}
