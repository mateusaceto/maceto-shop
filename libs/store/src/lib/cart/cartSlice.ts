import { CartType, ProductType } from '@maceto-shop/data-types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

const name = '@cart';

export type GenericStatus = 'loading' | 'success' | 'error';
export type GenericMethods = 'PUT' | 'STORE' | 'DELETE';

export interface State {
  data: Map<string, CartType>;
  getStatus: GenericStatus | null;
  setStatus: GenericStatus | null;
  error: string | null;
}

export interface GetSuccessPayload {
  data: Map<string, CartType>;
}

export interface GetFailurePayload {
  error: string | null;
}

export interface SetRequestPayload {
  product?: ProductType;
  quantity?: number;
  method: GenericMethods;
}

export interface SetSuccessPayload {
  data: Map<string, CartType>;
}
// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface SetFailurePayload extends GetFailurePayload {}

export const initialState: State = {
  data: new Map(),
  getStatus: 'loading',
  setStatus: null,
  error: null,
};

export const cartSlice = createSlice({
  name,
  initialState,
  reducers: {
    getRequest: (state) => {
      state.getStatus = 'loading';
    },
    getSuccess: (state: State, action: PayloadAction<GetSuccessPayload>) => {
      state.data = action.payload.data;
      state.getStatus = 'success';
    },
    getFailure: (state: State, action: PayloadAction<GetFailurePayload>) => {
      state.error = action.payload.error;
      state.getStatus = 'error';
    },
    setRequest: (state: State, _: PayloadAction<SetRequestPayload>) => {
      state.setStatus = 'loading';
    },
    setSuccess: (state: State, action: PayloadAction<SetSuccessPayload>) => {
      state.data = action.payload.data;
      state.setStatus = 'success';
    },
    setFailure: (state: State, action: PayloadAction<SetFailurePayload>) => {
      state.error = action.payload.error;
      state.setStatus = 'error';
    },
    setResetRequest: (state: State) => {
      state.setStatus = 'loading';
    },
    setResetSuccess: (state: State) => {
      state.getStatus = 'success';
      state.data = new Map();
    },
  },
});

export const {
  getRequest,
  getSuccess,
  getFailure,
  setFailure,
  setRequest,
  setSuccess,
  setResetSuccess,
  setResetRequest,
} = cartSlice.actions;

/**
 * @description Cart action Types
 */
export type CartTypes = keyof {
  [Property in keyof typeof cartSlice.actions as `${typeof name}/${Property}`]: null;
};

/**
 * @description Cart actions
 */

export type CartActionTypes = {
  [Property in keyof typeof cartSlice.actions]: PayloadAction<
    Parameters<typeof cartSlice.actions[Property]>[0],
    `${typeof name}/${Property}`
  >;
};
