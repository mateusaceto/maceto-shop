import { render, screen, waitFor } from '@testing-library/react';
import RootProvider from '../../../../providers';
import Produtos from '../index';
import { ProductType } from '@maceto-shop/data-types';

const MockProducts: React.FC = () => (
  <RootProvider>
    <Produtos />
  </RootProvider>
);

const ProductMockData: ProductType[] = [
  {
    id: '1',
    createdAt: '2019-09-02T12:58:54.103Z',
    name: 'Rustic Metal Fish',
    price: '289.00',
    image: 'http://lorempixel.com/640/480/food',
    stock: 65171,
  },
  {
    id: '2',
    createdAt: '2019-09-02T07:59:58.181Z',
    name: 'Sleek Wooden Soap',
    price: '430.00',
    image: 'http://lorempixel.com/640/480/transport',
    stock: 91260,
  },
  {
    id: '3',
    createdAt: '2019-09-02T22:14:05.454Z',
    name: 'Small Cotton Shoes',
    price: '993.00',
    image: 'http://lorempixel.com/640/480/sports',
    stock: 36608,
  },
];

const ProductsFaker = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve(ProductMockData),
  })
);

describe('Produtos tests', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<MockProducts />);

    expect(baseElement).toBeTruthy();
  });

  it('render header correctly', () => {
    render(<MockProducts />);
    const headerElement = screen.getByRole('heading');
    expect(headerElement).toBeTruthy();

    const headerTitleElement = screen.getByText('Maceto Shop - Produtos');
    expect(headerTitleElement).toBeTruthy();

    const cardButtonElement = screen.getByTestId('card-btn');
    expect(cardButtonElement).toBeTruthy();
  });

  it('render cards correctly', async () => {
    /* eslint-disable-next-line */
    const globalRef: any = global;
    globalRef.fetch = ProductsFaker;

    render(<MockProducts />);

    const cardElements = await waitFor(() =>
      screen.getAllByRole('contentinfo')
    );

    expect(cardElements).toHaveLength(3);

    let cardTitle = null;
    for (let i = 0; i < ProductMockData.length; i++) {
      cardTitle = await waitFor(() =>
        screen.getByText(ProductMockData[i].name)
      );
      expect(cardTitle).toBeTruthy();
    }

    const cardButtonElements = await waitFor(() =>
      screen.getAllByText('Adicionar ao carrinho')
    );

    const cardImageElements = await waitFor(() => screen.getAllByRole('img'));

    expect(cardImageElements).toHaveLength(3);
    expect(cardButtonElements).toHaveLength(3);
  });
});
