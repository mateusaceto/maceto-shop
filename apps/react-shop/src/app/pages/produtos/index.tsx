import { Box } from '@fower/react';
import { useCart, useProducts } from '@maceto-shop/react-hooks-lib';
import {
  Badge,
  GridList,
  IconButton,
  ProductCard,
} from '@maceto-shop/react-shop-ui';
import React from 'react';
import { FaShoppingCart } from 'react-icons/fa';
import { Link } from 'react-router-dom';
import DefaltLayout from '../../layouts/DefaltLayout';

const Produtos: React.FC = () => {
  const [data, { loading }] = useProducts();
  const [cart, { addToCart }] = useCart();

  return (
    <DefaltLayout
      title="Maceto Shop - Produtos"
      actions={
        <Link to="/carrinho" data-testid="card-btn">
          <Badge quantity={cart.size}>
            <IconButton icon={<FaShoppingCart />} />
          </Badge>
        </Link>
      }
    >
      {loading ? (
        <Box pt10 fontSans fontBold w="100%" textCenter>
          Carregando produtos...
        </Box>
      ) : (
        <GridList
          items={data}
          render={(product) => (
            <div role="contentinfo">
              <ProductCard
                {...product}
                onAdd={(quantity) => addToCart(product, quantity)}
              />
            </div>
          )}
        />
      )}
    </DefaltLayout>
  );
};

export default Produtos;
