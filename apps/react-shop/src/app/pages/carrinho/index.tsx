import { Box } from '@fower/react';
import { ProductType, toMoney } from '@maceto-shop/data-types';
import { useCart } from '@maceto-shop/react-hooks-lib';
import { Button, CartCard, GridList } from '@maceto-shop/react-shop-ui';
import React, { useMemo } from 'react';
import { Link, useHistory } from 'react-router-dom';
import DefaltLayout from '../../layouts/DefaltLayout';

const Carrinho: React.FC = () => {
  const [cart, { removeCartItem, changeCartQuantity, reset }] = useCart();
  const history = useHistory();

  const cardProducts: ProductType[] = useMemo(() => {
    if (cart.size === 0) return [];

    const mapEntries = Array.from(cart.entries());

    return mapEntries.map((interable) => {
      return { ...interable[1].product, quantity: interable[1].quantity };
    });
  }, [cart]);

  const totalPrice: number = useMemo(() => {
    return cardProducts.reduce((ac: number, product: ProductType) => {
      return ac + parseFloat(product.price) * (product?.quantity ?? 0);
    }, 0);
  }, [cardProducts]);

  return (
    <DefaltLayout
      title="Maceto Shop - Carrinho"
      actions={
        <Link to="/">
          <Button text="Continuar comprando" secondary />
        </Link>
      }
    >
      <GridList
        items={cardProducts}
        render={(product: ProductType) => (
          <div role="contentinfo">
            <CartCard
              {...product}
              onRemove={() => removeCartItem(product)}
              onChange={(value) => changeCartQuantity(product, value)}
            />
          </div>
        )}
      />
      <Box
        fixed
        shadowMedium
        w="100%"
        h="95"
        bgWhite
        bottom-0
        right-0
        row
        flex
        flexDirection="column"
        justifyContent="space-between"
        alignItems="center"
        black
        fontSans
        fontBold
        p2
        textXL
      >
        {`Total: ${toMoney(totalPrice.toString())}`}
        <Button
          text="ENVIAR PEDIDO"
          primary
          onClick={() => {
            history.goBack();
            reset();
          }}
        />
      </Box>
    </DefaltLayout>
  );
};

export default Carrinho;
