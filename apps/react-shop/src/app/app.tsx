import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Carrinho from './pages/carrinho';
import Produtos from './pages/produtos';

interface BaseRoutesTypes {
  path: string;
  component: React.ReactNode;
}

const BaseRoutes: BaseRoutesTypes[] = [
  {
    path: '/carrinho',
    component: <Carrinho />,
  },
  {
    path: '/',
    component: <Produtos />,
  },
];

const App: React.FC = () => {
  return (
    <Switch>
      {BaseRoutes.map((route, index) => (
        <Route path={route.path} key={`baseRoute${index}`}>
          {route.component}
        </Route>
      ))}
    </Switch>
  );
};

export default App;
