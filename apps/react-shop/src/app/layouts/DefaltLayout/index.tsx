import { Container, Header } from '@maceto-shop/react-shop-ui';
import { ToastContainer } from 'react-toastify';

interface DefaltLayoutProps {
  children: React.ReactNode | React.ReactNode[];
  title?: string;
  actions?: React.ReactNode;
}

const DefaltLayout: React.FC<DefaltLayoutProps> = ({
  children,
  title,
  actions,
}) => {
  return (
    <>
      <Header title={title} actions={actions} />
      <Container>
        {children}
        <ToastContainer position="top-left" autoClose={3300} />
      </Container>
    </>
  );
};

export default DefaltLayout;
