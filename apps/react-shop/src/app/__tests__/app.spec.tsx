import { render } from '@testing-library/react';
import RootProvider from '../../providers';
import App from '../app';

const MockApp: React.FC = () => (
  <RootProvider>
    <App />
  </RootProvider>
);

describe('App', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<MockApp />);

    expect(baseElement).toBeTruthy();
  });
});
