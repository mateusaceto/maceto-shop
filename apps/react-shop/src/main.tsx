import * as ReactDOM from 'react-dom';
import App from './app/app';
import RootProvider from './providers';
import 'react-toastify/dist/ReactToastify.css';

ReactDOM.render(
  <RootProvider>
    <App />
  </RootProvider>,
  document.getElementById('root')
);
