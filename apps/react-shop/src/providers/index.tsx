import { RootStore } from '@maceto-shop/store';
import React, { StrictMode } from 'react';
import { Provider as StoreProvider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import ThemeProvider from './ThemeProvider';

type Props = {
  children: React.ReactNode | React.ReactNode[];
};

const RootProvider: React.FC<Props> = ({ children }) => {
  return (
    <StrictMode>
      <StoreProvider store={RootStore}>
        <BrowserRouter>
          <ThemeProvider>{children}</ThemeProvider>
        </BrowserRouter>
      </StoreProvider>
    </StrictMode>
  );
};

export default RootProvider;
