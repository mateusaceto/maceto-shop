import React from 'react';
import { ThemeProvider as SCThemeProvider } from 'styled-components';
import { theme } from '@maceto-shop/data-types';

type Props = {
  children: React.ReactNode | React.ReactNode[];
};

const ThemeProvider: React.FC<Props> = ({ children }) => {
  return <SCThemeProvider theme={theme}>{children}</SCThemeProvider>;
};

export default ThemeProvider;
